#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    exec sudo /bin/bash "$0" "$@"
fi

echo "<> Configuring Power Management..."
sudo pmset -a tcpkeepalive 0
sudo pmset -a powernap 0
sudo pmset -a proximitywake 0
sudo pmset -a acwake 0
sudo pmset -a lidwake 0
sudo pmset -a ttyskeepawake 0

# Disable hibernation
# "To disable hibernation images completely, ensure hibernatemode standby and autopoweroff are all set to 0"
# echo "<> Disable hibernation..."
#sudo pmset -a hibernatemode 0
#sudo pmset -a standby 0
#sudo pmset -a autopoweroff 0

# Enable hibernation
echo "<> Enable hibernation..."
sudo pmset -a hibernatemode 25
sudo pmset -a standby 1
sudo pmset -a autopoweroff 0
sudo pmset -a standbydelayhigh 3600
sudo pmset -a standbydelaylow 3600
sudo pmset -a autopoweroffdelay 1800

echo "<> Rebuild kext..."
sudo spctl --master-disable
sudo mount -uw /
sudo killall Finder
sudo chown -v -R root:wheel /System/Library/Extensions
sudo touch /System/Library/Extensions
sudo chmod -v -R 755 /Library/Extensions
sudo chown -v -R root:wheel /Library/Extensions
sudo touch /Library/Extensions
sudo kextcache -i /

echo "<> Done, please reboot your macos!"
exit 0
