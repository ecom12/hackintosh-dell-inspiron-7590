# Hackintosh Dell Inspiron 7590
## Introduction
This is my very first Hackintosh. I'm pretty happy with the results so far. Everything seems pretty stable.
31-mar-2020: successful update to 10.15.4
05-feb-2021: successful update to 11.2
dec-2021   : successful update to 12.1 (Monterey)
07-feb-2022: successful update to 12.2 (Monterey)
18-mar-2024: successful update to 14.4 (Sonoma)
20-dec-2024: successful update to 15.1.1 (Sequoia)

## Thanks!
This is almost the most important point of this page: THANK YOU to the entire Hackintosh community for making this possible and providing the required help and support!

I do recognise all the effort required to build and maintain this. Thank you everybody!

Now, let's go to more serious and dig into my config.

Almost everything is working now and my installation is very stable.

This README contains a bunch of notes I took during my tests and experimentations. It should be up to date.

Note: I am NOT a Hackintosh expert. This is my first hack, and this is just MY personal understanding of this very complex world, so there may be errors or incorrect information. Still, I hope it will be useful.

# OpenCore build
## OpenCore

As I'm not able to do it from scratch by myself, my starting point is on tctien342's work here: https://github.com/tctien342/Dell-Inspiron-7591-Hackintosh
I'm starting from his tag opencore-v4.5 and I'll update it for my configuration, which is very similar. This initial commit is exactly the same from tctien342, this is mainly to track my changes.
Thank you tctien342!

## Disabled - Not supported - Not working
* digit reader (will never works, from what I know) - Disabled with USBPatch
* internal mic
* HDMI/DP audio out: I have never been able to get audio out working on the video link (DP or HDMI)

## USB Configuration
So I understand there are 2 ways to configure USB ports:
* Create a USBPorts.kext, as described here: https://www.tonymacx86.com/threads/the-new-beginners-guide-to-usb-port-configuration.286553/
* Use USBInjectAll.kext along with customized SSDT-UIAC.aml

Right now I'm using the first option, which seems to be the one recommended.

Here are the ports I've discovered on my Inspiron 7590:
* External ports:
  * Right      : HS01/SS01
  * Left-far   : HS02/SS02
  * Left near  : HS03/SS03
  * Left type-C: HS04
* Internal ports:
  * HS09: Finger print (disabled in my USBPorts.kext)
  * HS05: WebCam
  * HS06: SDCard Reader
  * HS14: Bluetooth Broadcom

## Wifi / Bluetooth - DW1820a
I initially bought a DW1560 which is considered to have better compatibility (from what I've read) but I was not able to install it into my Dell Inspiron 7590 because the antenna connectors are located differently and the antenna cable are too short to reach the connectors. So I ended up with a DW1820A, it took me some time to find a vendor with the correct reference (many clones are available on the market).
* Here some source of information:
  * https://github.com/acidanthera/BrcmPatchRAM
  * https://www.tonymacx86.com/threads/the-solution-dell-dw1820a-broadcom-bcm94350zae-macos-15.288026/
  * https://osxlatitude.com/forums/topic/11322-broadcom-bcm4350-cards-under-high-sierramojavecatalina/?tab=comments#comment-88597
* Installation:
  * Need to tape some pins. Without this my hackintosh was stalling during boot
  * Do not use boot option brcmfx-driver
  * I only added the following properties:
	<key>PciRoot(0x0)/Pci(0x1d,0x6)/Pci(0x0,0x0)</key>
	<dict>
		<key>compatible</key>
		<string>pci14e4,4353</string>
		<key>pci-aspm-default</key>
		<integer>0</integer>
	</dict>
  * In order to find the PCI path:
    * use Hackintool
    * go to "PCI" tabs, sort by "Device Name"
    * Find "BCM4350...", check device path on the right
    * Right click to copy the IOReg path into clipboard
* Wifi:
  * no issue, works perfectly
  * With Sonoma, had to disable SIP
* Bluetooth
  * Bluetooth now works, even after hibernate & sleep.
  * Note: Sonoma/Sequoia: after sleeping, BT needs to be disabled/Enabled. (Done with BlueSnooze appication)

## Video
* HDMI ouput works, but I was not able to get audio out and 4k is only 30Hz
* USB-C -> HDMI works as well, but only 4k@30Hz
* USB-C -> Display Port: allows 4k@60Hz, but not working after boot: need to sleep/wake once to get it working.

4k@60Hz seems to be a challenge. I am using USB-C/DisplayPort now. It works most of the time, but sometimes, after update or reboot it defaults back to 30Hz.

## Audio
* No issue with audio, except internal mic not working

## Power management
I have disabled CPUFriend.

## Sleep / Wakeup / Hibernation
Sleep / Wakeup works without any issue.
Hibernation works most of the time.
But: does not work when pluged to a power source. In this case, I get into a reboot loop: the laptop hibernates properly, but then wakes up by itself, which is strange because I configured everything to disable "wake-on-power"

I initially had AddressBookSync preventing auto-sleep
I fixed this by enabling address book sync to iCloud, even if it's empty and I don't have data there.
(Don't know if this is still needed)

## BIOS configuration
Still to be documented...

* Thunderbolt: disabled auto-switch and force 'Native enumeration'.
  Still to confirm if thunderbolt device (dockstation WD15) is better detected...
  After rebooting all is ok.

# History
## EFI.029 (Sequoia)
* [OC]
  * Update for Sequoia. Everything OK (Including BT, Wifi, Hibernation)
  * Requires Bluesnooze install to enable BT at login (same as Sonoma)

## EFI.028 (Sonoma)
* [OC]
  * fix dark screen (internal monitor) during 1 minute after boot is back.

## EFI.027 (Sonoma)
* [OC]
  * OpenCore 0.9.9
  * known issue: dark screen (internal monitor) during 1 minute after boot is back.

## EFI.025 (Monterey)
* [OC]
  * OpenCore 0.8.2
  * About 4k@60Hz: does not seem possible while internal monitor is enabled. I'm using ResXtreme app to configure 2k@60Hz and this works fine.
  * Update hibernation configuration. Works fine.
  * fix dark screen during 1 minute after boot
  * cleanup in config.plist
  * cleanup: remove unused kexts
## EFI.024 (Monterey)
* [OC]
  * OpenCore 0.7.8
  * Update kexts
  * Change video card ID (still fighting to get robust 4K@60Hz)
## EFI.023 (Monterey)
* [OC]
  * Now running on MacOS Monterey
  * Disable BrcmBluetoothInjector for Monterey compatibility (refer to https://github.com/acidanthera/bugtracker/issues/1669)
    * Replaced by BlueToolFixup to fix bluetooth
  * Update kexts
  * Update config.plist based on checker output
  * Issues:
    * 4k@60Hz des not seem to work...
    * Bluetooth may not work after wake-up or boot
## EFI.022 (Big Sur)
* [OC]
  * OpenCore 0.7.6
  * Update kexts
## EFI.021 (Big Sur)
* [OC]
  * OpenCore 0.7.3
  * Update kexts
## EFI.020 (Big Sur)
* [OC]
  * OpenCore 0.7.1
  * Update kexts
## EFI.019 (Big Sur)
* [OC]
  * OpenCore 0.6.8
  * Update kexts
  * Finally fixed "mp_kdp_enter() timed-out during locked wait after NMI" !!
    * No more KP while sleeping, no more KP at all, everything is very stable now.
    * Update cpuTscSync to latest (https://github.com/acidanthera/CpuTscSync/releases)
    * Refer to this thread: https://github.com/tctien342/Dell-Inspiron-7591-Hackintosh/issues/7
  * Note: still using an old version of WEG because since 1.4.6, at boot, the laptop screen brightness is minimal for ~1min, and then becomes normal.
  
## EFI.018 (Big Sur)
* [OC]
  * OpenCore 0.6.6
  * /!\ Sleep/Wakeup: Still not stable.
  * Update USBPorts.kext for MacBookPro16,1 (to match actual configuration)
  * Update kexts
  * Update boot args (removed darkwake)
  * Update video configuration (not tested on 4K monitor yet). platform-id now same as reported by gxfutil (3E9B)
  * More ACPI fixes: now using exact same ACPI config as tctien
  * Update patches (now same as tctien)
  * Good news: fans are no more always ON !
  * Can't boot Windows 10 from OC, but Windows boots fine when called directly from the BIOS (bypass OC) 
  * Note: also ugpraded the BIOS in meanwhile (not on purpose, update intiated by Windows 10 update...)

## EFI.017 (Big Sur)
* [OC]
  * Remove ACPI updates introduced in EFI.015.
  * Stability is really improved, but still experiencing crashes from time to time

## EFI.016 (Big Sur)
* [OC]
  * Disable verbose boot
  * Note: lid wake-up is not robust and often causes crashes

## EFI.015 (Big Sur)
* [OC]
  * Enable SIP (System Integreity Protection)
  * ACPI updates as recommended by Dortania's guide
    * https://dortania.github.io/OpenCore-Install-Guide
  * checked with https://opencore.slowgeek.com/
  * Note: lid wake-up is not robust and often causes crashes

## EFI.014 (Big Sur)
* [OC]
  * Update for Big Sur: clean-up EFI drivers and fixed boot-args

## EFI.013
Oups, didn't update history since a long time...
* [OC]
  * Prepare for Big Sur, based on latest updates from tctien
  * Update to OpenCore 0.63
  * Update all kexts
  * Note: Very stable, ran for 2 monthes non-stop (just sleep/wakeup).

## EFI.012
* [OC]
  * Update to OpenCore 0.60
  
## EFI.011
* [OC]
  * Fix audio
  
## EFI.010
* [OC]
  * Update to OpenCore 0.58
  * Update from tctien

## EFI.009
## EFI.008

## EFI.007
* [OC]:
  * clean-up boot
  * clean-up config.plist
  * update kexts
  * same behavior than EFI.006
* [Clover]: no change

## EFI.006
* [OC] Add OpenCore build based on tctien342
  * Initial tests are pretty good, I didn't test everything yet, but main features/devices seems to work same as in Clover:
  * Working:
    * USB-C: ok with Eth, HDMI, USB
    * SDCard: ok
    * Internal speaker: ok
    * Internal webcam: ok
    * Combo jack: ok (did not test mic)
    * DW1820 Wifi + Bluetooth: ok
    * Sleep: ok (Lid wake-up disabled)
      * lid-wakeup ok
      * auto-sleep: ok (had to enable address book sync to iCloud)
      * Sleep 12h with USB-C Eth/hdmi/usb ==> 91%->31%
  * Not working:
    * internal mic: NOK
  * Known issues:
    * Brightness is at max when waking up from sleep
    * Sleep robustness: seems to crash during sleep from time to time. I found the laptop with fans on and keyboard light on, no screen, had to force power-off
  * Not tested:
    * HDMI out
  * Disabled:
    * Fingerprint reader
    * Hibernation

* [Clover]: Audio config update, had-verb not needed anymore

## EFI.005
* Fixed HDMI output
* kext updates

## EFI.004
* Added Wifi and Bluetooth support with DW1820A
* kext updates
* Clover update
* Clean-up

## EFI.003
This configuration still requires some clean-up and adjustments, but it really is very stable. No crash observed so far and most features working.
* Switch to VirtualSMC
* Add USBPort.kExt
* Revert VoodooPS2Controller to 1.9.2: more stable now
* Update CodecCommander to latest
* changed AppleALC audio layout configuration
* Some clean up in:
  * installed kexts
  * ACPI patches

## EFI.002
* Update Clover to 5099
* Update most kexts

## EFI.001


# My Personnal Notes...
## Useful links
* Lilu: https://www.tonymacx86.com/threads/an-idiots-guide-to-lilu-and-its-plug-ins.260063/
* Laptops: https://www.tonymacx86.com/threads/guide-booting-the-os-x-installer-on-laptops-with-clover.148093/
* USB Guide: https://www.tonymacx86.com/threads/the-new-beginners-guide-to-usb-port-configuration.286553/
* ACPI patches: https://www.tonymacx86.com/threads/guide-patching-laptop-dsdt-ssdts.152573/
* DarkWake: https://www.insanelymac.com/forum/topic/342002-darkwake-on-macos-catalina-boot-args-darkwake8-darkwake10-are-obsolete/
* Intel Graphics: https://github.com/acidanthera/WhateverGreen/blob/master/Manual/FAQ.IntelHD.en.md
* HDMI/Framebuffers: https://www.tonymacx86.com/threads/guide-general-framebuffer-patching-guide-hdmi-black-screen-problem.269149/
* HDMI Audio:
  * https://www.tonymacx86.com/threads/guide-intel-igpu-hdmi-dp-audio-all-sandy-bridge-kaby-lake-and-likely-later.189495/
* CPU Management: https://github.com/tctien342/smart-cpu
  * To be tested

## kExts
* SMCSuperIO (Lilu): for temperature monitoring
* SMCProcessor (Lilu): temperature monitoring
* NoTouchID: disable finger print reader (improve boot time)
* cpuTscSync: required to fix "timed-out during locked wait after NMI" - https://github.com/acidanthera/CpuTscSync
  * Note: Hackintool does not check this kext => don't forget to check it manually
* VoodooI2C: trackpads & touchscreens - https://www.tonymacx86.com/threads/voodooi2c-help-and-support.243378/
* VoodooPS2Controller: mouse, etc
* AppleALC: required for Audio
* CodecCommander: not clear what it does... Seems to be for headphone... Use EAPD version from Rehabman
  * seems that it's not required anymore (source: https://www.tonymacx86.com/threads/help-alc295-no-sound-through-headphones-with-applealc.257549/page-3)
  * For now, I still need it, along with ALCPlugFix to get headphone audio working...
  * 27-feb-20: Removed. Now using patched AppleALC.kext and had-verb is not needed anymore for headphone
* USBInjectAll: can be used temporarily to configure USB ports. Not intended to be loaded on a stable configuration
* AirportBrcmFixup: for wifi support (Airport Broadcom)
* CPUFriend: used to patch CPU Low Frequency Mode
* NullEthernet: installed in /L/E, now AppStore works, as expected ;) It was not loading from Clover.
  * I used it before installing DW1820A Wifi module.
  * Now I do not use it anymore
* HibernationFixup.kext (Lilu): Enables 'native' hibernation
* AirportBrcmFixup.kext: required for Bluetooth
* BrcmPatchRAM3.kext: for Broadcom Bluetooth
* BrcmFirmwareData.kext: for Bluetooth
* BrcmBluetoothInjector.kext: for Bluetooth

## ACPI patches

As this config is based on someone else config (with same HW), I don't know what are all the ACPI patches here. Trying to list them and understand if they are really needed or not...
They are all SSDT files.

* SSDT-ADP1: Power management (Battery status ?)
** does it work without "fixADP1" in clover ?

* SSDT-BRT6: Fixes internal keyboard brightness keys.
* SSDT-DDGPU: Disable the discrete GPU
* SSDT-DMAC: Inject the DMA controller information which macOS default search using SSDT-DMAC.aml
  * Is it required ?

* SSDT-HDEF.dsl
  * This file is huge! related to Dell Audio. No idea what's its purpose...
  * Maybe it's for HDMI audio.

* SSDT-LANC_PRW: fixes some "instant wake" issues
  * https://blog.cloudops.ml/Clover-ACPI-hotpatch.html

* SSDT-LPC.aml: Require. Boot fails if I remove it.
  * LPC stand for Low Pin Count - it is the chip used to connect all of the "legacy" PC components on motherboards. For example it will control the PS/2, floppy, parallel and serial ports.

* SSDT-PNLFCFL.aml: Used for backlight control
  * Required. No backlight control if I remove it

* SSDT-PPMC.aml: no idea! TODO: remove it

* SSDT-PTSWAK: Sleep and Wake (PTS=Prepare To Sleep)

* SSDT-RMCF.aml: contains various configuration params used by other ACPI patches
  * required by SSDT-PTSWAK
  * Source: https://github.com/RehabMan/OS-X-Clover-Laptop-Config/tree/master/hotpatch

* SSDT-RMNE.aml: Add NULL Ethernet port
  * Required by NullEthernet.kext

* SSDT-SMBUS.aml: Don't know what it does. Related to touchpad?
  * TODO: try to remove it

* SSDT-UIAC.aml:
  * USB Inject All Configuration
  * I don't use USBInjectAll, I clearly don't need it

* SSDT-UPRW.aml: Needed to solve 'Instant Wake' issues. (Don't know what that is)

* SSDT-USBX.aml:
  * Required for USB powered ??
  * ref: https://www.tonymacx86.com/threads/guide-usb-power-property-injection-for-sierra-and-later.222266/

* SSDT-XHC.aml: seems required, but don't know what it does. About USB2

* SSDT-XOSI.aml: Simulates Windows. Required

## Useful Commands
* > sudo kextstat: list all loaded kexts
* > kextcache: can be used to clear cache of LOCAL kext only (/L/E)
* > pmset -g assertions: check if auto sleep is allowed
* > pmset -g: displays power settings
* > pmset -g log | grep -e "Wake.*due to ": Wake reason
* > pmset -g log | grep -e " Sleep  " -e " Wake  " -e " DarkWake  " -e "Hiber": Display sleep/wake history
* > pmset -g sched: list power management scheduled activities
* Logging:
  * > log show --style syslog --start "2019-12-22 09:30:00" | fgrep "Wake reason"


## Power management
* Safe Sleep / autopoweroff: https://support.apple.com/guide/mac-help/what-is-safe-sleep-mh10328/mac
* standby: https://support.apple.com/en-us/HT202124


## Acronyms & definitions
* ALS: Ambient Light Sensors
* PRW: Power Resources for Wake
* RMNE: RehabManNullEthernet ??
* RMCF: RehabManConFig ??
* SIP: System Integrity Protection
* EHC: USB2
* XHC: USB3
* LSPCon: Level Shifter - Protocol Converter: hw component which creates HDMI output signal from intel graphics chipset
  * some info here: https://www.tonymacx86.com/threads/guide-intel-framebuffer-patching-using-whatevergreen.256490/
